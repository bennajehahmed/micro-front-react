import "./App.css";
import { Card } from "antd";
import { useEffect, useState } from "react";
import axios from "axios";

function App() {
  let [bill, setBill] = useState(null);
  useEffect(() => {
    axios({
      method: "get",
      url: "http://dbc4-105-66-131-59.ngrok.io/BILLING-SERVICE/bills/full/1",
    }).then(function (response) {
      response.data.productItems = response.data.productItems.map((i) => ({
        quantity: i.quantity,
        ...i.product,
      }));
      console.log(response.data);
      setBill(response.data);
    });
  }, []);
  return (
    bill && (
      <Card
        title={"Bill number : " + bill.id}
        style={{ width: "80%", marginInline: "auto" }}
      >
        <h3>Billing Date : </h3>
        <p>{bill.billingDate}</p>
        <h3>Products : </h3>
        <table>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
          {bill.productItems.map((elem) => (
            <tr>
              <td>{elem.id}</td>
              <td>{elem.name}</td>
              <td>{elem.price}</td>
              <td>{elem.quantity}</td>
            </tr>
          ))}
        </table>
        <h3>Customer : </h3>
        <Card>
          <p>Id : {bill.customer.id}</p>
          <p>Name : {bill.customer.name}</p>
          <p>Email : {bill.customer.email}</p>
        </Card>
      </Card>
    )
  );
}

export default App;
